# -*- coding: utf-8 -*-

from conans import ConanFile, CMake, tools
import os


class NCNNConan(ConanFile):
    name = "ncnn"
    lib_version = "20200106"
    revision = '0'
    version = lib_version + '-' + revision
    homepage = "https://github.com/Tencent/ncnn"
    description = "NCNN network inference library"
    topics = ("conan", "ncnn")
    author = "Invision AI"
    generators = "cmake"
    settings = "os", "arch", "compiler", "build_type"
    options = {"with_opencv": [True, False], 
               "with_openmp": [True, False], 
               "fPIC": [True, False], 
               "avx2": [True, False],
               "benchmark": [True, False],
               "requant": [True, False],
               }

    default_options = {'fPIC': True,
                       'with_openmp': True,
                       'with_opencv': True,
                       'avx2': False,
                       'benchmark': False,
                       'requant': False,
                       }

    _source_subfolder = "source_subfolder"

    def system_requirements(self):
        if self.settings.os == 'Linux' and tools.os_info.is_linux:
            if tools.os_info.with_apt:
                installer = tools.SystemPackageTool()

                packages = []

                if self.options.with_openmp and self.settings.compiler == 'clang':
                    packages.append('libomp-dev')

                for package in packages:
                    installer.install(package)

    def requirements(self):
        self.requires("protobuf/[>=3.9.1]@bincrafters/stable")

        # the protobuf package above does not provide the protoc binary, so we need the one below
        self.requires("protoc_installer/[>=3.9.1]@bincrafters/stable")

        if self.options.with_opencv:
            self.requires("opencv/[>=4.1.1-1]@invisionai/stable")

    def source(self):
        sha256 = "67101dc8e15659c48fa2e28fe604cf4ae3e704b873a98132837cef079b08e1ce"
        tools.get("{0}/archive/{1}.tar.gz".format(self.homepage, self.lib_version), sha256=sha256)
        extracted_dir = self.name + "-" + self.lib_version
        os.rename(extracted_dir, self._source_subfolder)

        tools.replace_in_file("{0}/CMakeLists.txt".format(self._source_subfolder),
                              'project(ncnn)',
                              'project(ncnn)\ninclude(../conanbuildinfo.cmake)\nconan_basic_setup(TARGETS)')

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.definitions['NCNN_OPENCV'] = self.options.with_opencv
        cmake.definitions['NCNN_AVX2'] = self.options.avx2
        cmake.definitions['NCNN_BUILD_TESTS'] = False
        cmake.definitions['NCNN_BENCHMARK'] = self.options.benchmark
        cmake.definitions['NCNN_DISABLE_PIC'] = not self.options.fPIC
        cmake.definitions['NCNN_OPENMP'] = self.options.with_openmp
        cmake.definitions['NCNN_REQUANT'] = self.options.requant

        cmake.configure(source_dir=self._source_subfolder)
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)
        if self.settings.os == "Linux":
            self.cpp_info.system_libs.extend(['pthread', 'm'])
            if self.options.with_openmp:
                self.cpp_info.cppflags = ["-fopenmp"]
                self.cpp_info.sharedlinkflags = ["-fopenmp"]

